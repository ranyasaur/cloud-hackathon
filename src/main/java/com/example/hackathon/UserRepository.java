package com.example.hackathon;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {
    
    @PersistenceContext(name="persistentUnit")
    protected EntityManager entityManager;

    public long getUserCount() throws DataAccessException {
        String jpql = "select count(u) from User u";
        TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
        return query.getSingleResult();
    }

    //get specific user details
    public User getUser(long userId) throws DataAccessException {
        return entityManager.find(User.class, userId);
    }

    //get list of all users
    public List<User> getAllUsers() throws DataAccessException {
        String jpql = "select u from User u";
        TypedQuery<User> query = entityManager.createQuery(jpql,User.class);
        return query.getResultList();

    }

    @Transactional
    public void insertUser(User u){
        entityManager.persist(u);
    }

    //update users particulars
    //cannot update userid
    @Transactional
    public void updateUser(long id, User u) {
        User userUpdated = entityManager.find(User.class, id);
        userUpdated.setUsername(u.getUsername());
        userUpdated.setPassword(u.getPassword());
    }

    //delete user
    @Transactional
    public void deleteUser(User u) {
        User userDelete = entityManager.find(User.class, u.getUserId());
        entityManager.remove(userDelete);
    }
}
